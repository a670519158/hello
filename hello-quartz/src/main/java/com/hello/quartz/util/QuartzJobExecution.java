package com.hello.quartz.util;

import org.quartz.JobExecutionContext;
import com.hello.quartz.domain.SysJob;

/**
 * 定时任务处理（允许并发执行）
 * 
 * @author hello
 *
 */
public class QuartzJobExecution extends AbstractQuartzJob
{
    @Override
    protected void doExecute(JobExecutionContext context, SysJob sysJob) throws Exception
    {
        JobInvokeUtil.invokeMethod(sysJob);
    }
}
