package com.hello.custom.mapper;

import com.hello.custom.domain.SysCustomField;
import com.hello.custom.domain.SysCustomList;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 自定义列Mapper接口
 *
 * @author lyj
 * @date 2020-12-17
 */
public interface SysCustomListMapper {
    /**
     * 查询自定义列
     *
     * @param id 自定义列ID
     * @return 自定义列
     */
    public SysCustomList selectSysCustomListById(Long id);

    /**
     * 查询自定义列列表
     *
     * @param sysCustomList 自定义列
     * @return 自定义列集合
     */
    public List<SysCustomList> selectSysCustomListList(SysCustomList sysCustomList);

    /**
     * 新增自定义列
     *
     * @param sysCustomList 自定义列
     * @return 结果
     */
    public int insertSysCustomList(SysCustomList sysCustomList);

    /**
     * 修改自定义列
     *
     * @param sysCustomList 自定义列
     * @return 结果
     */
    public int updateSysCustomList(SysCustomList sysCustomList);

    /**
     * 删除自定义列
     *
     * @param id 自定义列ID
     * @return 结果
     */
    public int deleteSysCustomListById(Long id);

    /**
     * 批量删除自定义列
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysCustomListByIds(String[] ids);

    /* *
     *@description: 自定义sql
     *@author: lyj
     *@time: 2020/12/18 11:17
     *@param [listSQL]
     */
    public List<Map<String,Object>> executeCustomSql(@Param("customSql") String customSql);
}
