package com.hello.custom.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.hello.common.core.text.Convert;
import com.hello.common.exception.BusinessException;
import com.hello.common.utils.DateUtils;
import com.hello.common.utils.StringUtils;
import com.hello.custom.domain.SysCustomButton;
import com.hello.custom.domain.SysCustomField;
import com.hello.custom.domain.SysCustomList;
import com.hello.custom.mapper.SysCustomListMapper;
import com.hello.custom.service.ISysCustomListService;
import com.hello.framework.util.ShiroUtils;
import com.hello.system.domain.SysDictData;
import com.hello.system.utils.DictUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * 自定义列Service业务层处理
 *
 * @author lyj
 * @date 2020-12-17
 */
@Service
@Transactional
@Slf4j
public class SysCustomListServiceImpl implements ISysCustomListService {

    @Resource
    private SysCustomListMapper sysCustomListMapper;


    /**
     * 查询自定义列
     *
     * @param id 自定义列ID
     * @return 自定义列
     */
    @Override
    public SysCustomList selectSysCustomListById(Long id) {
        return sysCustomListMapper.selectSysCustomListById(id);
    }

    /**
     * 查询自定义列列表
     *
     * @param sysCustomList 自定义列
     * @return 自定义列
     */
    @Override
    public List<SysCustomList> selectSysCustomListList(SysCustomList sysCustomList) {
        return sysCustomListMapper.selectSysCustomListList(sysCustomList);
    }

    /**
     * 新增自定义列
     *
     * @param sysCustomList 自定义列
     * @return 结果
     */
    @Override
    public int insertSysCustomList(SysCustomList sysCustomList) {
        makeCustomListData(sysCustomList);
        sysCustomList.setCreateTime(DateUtils.getNowDate());
        return sysCustomListMapper.insertSysCustomList(sysCustomList);
    }

    /**
     * 修改自定义列
     *
     * @param sysCustomList 自定义列
     * @return 结果
     */
    @Override
    public int updateSysCustomList(SysCustomList sysCustomList) {
        makeCustomListData(sysCustomList);
        sysCustomList.setUpdateTime(DateUtils.getNowDate());
        return sysCustomListMapper.updateSysCustomList(sysCustomList);
    }

    //组装数据
    private void makeCustomListData(SysCustomList sysCustomList) {
        List<SysCustomField> columns = sysCustomList.getColumns();
        Collections.sort(columns, new Comparator<SysCustomField>() {
            public int compare(SysCustomField o1, SysCustomField o2) {
                return o1.getSort().compareTo(o2.getSort());
            }
        });
        for (SysCustomField sysCustomField : columns) {
            sysCustomField.setColumnName(sysCustomField.getColumnName().trim());
            sysCustomField.setColumnShowName(sysCustomField.getColumnShowName().trim());
        }

        if (columns.size() > 0) {
            sysCustomList.setListValue(JSONUtil.toJsonStr(columns));
        }
        String listSql = sysCustomList.getListSql().replace("\r", " ")
                .replace("\n", " ");
        sysCustomList.setListSql(listSql);
    }

    /**
     * 删除自定义列对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSysCustomListByIds(String ids) {
        return sysCustomListMapper.deleteSysCustomListByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除自定义列信息
     *
     * @param id 自定义列ID
     * @return 结果
     */
    @Override
    public int deleteSysCustomListById(Long id) {
        return sysCustomListMapper.deleteSysCustomListById(id);
    }

    @Override
    public List<SysCustomField> executeCustomSql(String listSql, Long id) throws BusinessException {
        List<SysCustomField> oldFields = new ArrayList<>();
        //验证sql
        try {
            List<Map<String, Object>> list = sysCustomListMapper.executeCustomSql(listSql);
        } catch (Exception e) {
            throw new BusinessException("sql错误");
        }
        //获取列表字段
        String[] cellName = listSql.toLowerCase().split("from")[0]
                .replace("select", "")
                .replace("\r", "")
                .replace("\n", "")
                .replace("\t", "")
                .split(",");
        //如果已存
        if (id != null) {
            SysCustomList oldData = sysCustomListMapper.selectSysCustomListById(id);
            String listValue = oldData.getListValue();
            if (StringUtils.isNotEmpty(listValue)) {
                oldFields = JSONUtil.toList(JSONUtil.parseArray(listValue), SysCustomField.class);
            }
        }

        //组装字段列表
        List<SysCustomField> fieldList = new ArrayList<>();
        for (String str : cellName) {
            SysCustomField sysCustomField = new SysCustomField();
            sysCustomField.setColumnName(str.trim());
            sysCustomField.setSort("");
            sysCustomField.setDictType("");
            sysCustomField.setColumnShowName(str);
            if (oldFields.size() > 0) {
                for (SysCustomField old : oldFields) {
                    if (old.getColumnName().equals(str.trim())) {
                        sysCustomField.setIsHide(old.getIsHide());
                        sysCustomField.setIsUse(old.getIsUse());
                        sysCustomField.setIsSearch(old.getIsSearch());
                        sysCustomField.setSort(old.getSort());
                        sysCustomField.setDictType(old.getDictType());
                        sysCustomField.setColumnShowName(old.getColumnShowName());
                    }
                }
            }
            fieldList.add(sysCustomField);
        }
        Collections.sort(fieldList, new Comparator<SysCustomField>() {
            public int compare(SysCustomField o1, SysCustomField o2) {
                return o1.getSort().compareTo(o2.getSort());
            }
        });

        return fieldList;
    }

    @Override
    public int changeStatus(SysCustomList sysCustomList) {
        return sysCustomListMapper.updateSysCustomList(sysCustomList);
    }

    @Override
    public SysCustomList selectSysCustomListByCode(String listCode) {
        SysCustomList sysCustomList = new SysCustomList();
        sysCustomList.setListCode(listCode);
        List<SysCustomList> list = sysCustomListMapper.selectSysCustomListList(sysCustomList);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public List<Map<String, Object>> selectSysCustomListData(SysCustomList sysCustomList) {
        String listSql = selectSysCustomListByCode(sysCustomList.getListCode()).getListSql();
        //组装查询条件
        listSql += " where 1=1 and del_flag=0 ";
        if (StrUtil.isNotEmpty(sysCustomList.getListQuery())) {
            String listQuery = sysCustomList.getListQuery();
            JSONArray array = JSONUtil.parseArray(listQuery);
            for (Object object : array) {
                JSONObject obj = (JSONObject) object;
                String name = obj.getStr("name");
                String value = obj.getStr("value");
                if (StrUtil.isNotEmpty(value)) {
                    listSql += " and " + name + " like '%" + value + "%'";
                }
            }
        }
        List<Map<String, Object>> list = sysCustomListMapper.executeCustomSql(listSql);
        return list;
    }

    @Override
    public String queryDictValueByType(String dictType) {
        List<SysDictData> dictData = DictUtils.getDictCache(dictType);
        if (dictData.size() > 0) {
            JSONArray array = new JSONArray();
            for (SysDictData dict : dictData) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("lable", dict.getDictLabel());
                jsonObject.put("value", dict.getDictValue());
                array.add(jsonObject);
            }
            return array.toString();
        }
        return "";
    }

    @Override
    public int customFormSaveOrUpdate(SysCustomList sysCustomList) {
        String form = sysCustomList.getListForm();
        try {
            form = new String(Base64.getDecoder().decode(form), "utf-8");
            JSONArray jsonArray = JSONUtil.parseArray(form);
            for (Object object : jsonArray) {
                JSONObject jsonObject = (JSONObject) object;
                //表名
                String table = jsonObject.getStr("table");
                //主键
                String id = jsonObject.getStr("id");
                //新增修改
                String type = jsonObject.getStr("type");
                StringBuffer sql = new StringBuffer("insert into " + table);
                if ("update".equals(type)) {
                    sql = new StringBuffer("update " + table + " set ");
                }

                //当前用户
                String userName = ShiroUtils.getSysUser().getLoginName();
                //明细
                JSONArray formDataArray = (JSONArray) jsonObject.get("formData");
                List<String> updateList = new ArrayList<>();
                updateList.add("update_time='" + DateUtil.now() + "'");
                updateList.add("update_by='" + userName + "'");
                List<String> insertValueList = new ArrayList<>();
                List<String> insertNameList = new ArrayList<>();
                insertNameList.add("create_time");
                insertValueList.add(DateUtil.now());
                insertNameList.add("create_by");
                insertValueList.add(userName);
                for (Object formObj : formDataArray) {
                    JSONObject formJson = (JSONObject) formObj;
                    String name = formJson.getStr("name");
                    String value = formJson.getStr("value");
                    if ("id".equals(name)) {
                        continue;
                    }
                    if ("update".equals(type)) {
                        updateList.add(name + "='" + value + "'");
                    } else {
                        insertNameList.add(name);
                        insertValueList.add("'" + value + "'");
                    }
                }
                if ("update".equals(type)) {
                    sql.append(updateList.toString().replace("[", "").replace("]", ""));
                    sql.append(" where id='" + id + "'");
                } else {
                    sql.append(insertNameList.toString().replace("[", "(").replace("]", ")"));
                    sql.append("values ");
                    sql.append(insertValueList.toString().replace("[", "(").replace("]", ")"));
                }
                sysCustomListMapper.executeCustomSql(sql.toString());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            return 0;
        }
        return 1;
    }

    @Override
    public List<SysCustomButton> customButton(Long id) {
        List<SysCustomButton> customButtons = new ArrayList<>();
        SysCustomList customList = sysCustomListMapper.selectSysCustomListById(id);
        if (customList != null && StrUtil.isNotEmpty(customList.getListButton())) {
            JSONArray jsonArray = JSONUtil.parseArray(customList.getListButton());
            customButtons = JSONUtil.toList(jsonArray, SysCustomButton.class);
            Collections.sort(customButtons, new Comparator<SysCustomButton>() {
                @Override
                public int compare(SysCustomButton o1, SysCustomButton o2) {
                    Integer n1 = StrUtil.isEmpty(o1.getSort()) ? 0 : Integer.parseInt(o1.getSort());
                    Integer n2 = StrUtil.isEmpty(o2.getSort()) ? 0 : Integer.parseInt(o2.getSort());
                    return n1 - n2;
                }
            });
        }
        return customButtons;
    }

    @Override
    public int customButtonSave(SysCustomButton sysCustomButton) {
        List<SysCustomButton> customButtons = new ArrayList<>();
        List<SysCustomButton> customButtonsSave = new ArrayList<>();
        if (StrUtil.isNotEmpty(sysCustomButton.getCustomId())) {
            SysCustomList customList = sysCustomListMapper.selectSysCustomListById(Long.parseLong(sysCustomButton.getCustomId()));
            if (customList != null) {
                if (StrUtil.isNotEmpty(customList.getListButton())) {
                    JSONArray jsonArray = JSONUtil.parseArray(customList.getListButton());
                    customButtons = JSONUtil.toList(jsonArray, SysCustomButton.class);
                }

                if (StrUtil.isNotEmpty(sysCustomButton.getButtonId())) {
                    //修改按钮
                    for (SysCustomButton button : customButtons) {
                        if (button.getButtonId().equals(sysCustomButton.getButtonId())) {
                            customButtonsSave.add(sysCustomButton);
                        } else {
                            customButtonsSave.add(button);
                        }
                    }
                    customButtons = customButtonsSave;
                } else {
                    //添加按钮
                    sysCustomButton.setButtonId(IdUtil.simpleUUID());
                    customButtons.add(sysCustomButton);
                }
                Collections.sort(customButtons, new Comparator<SysCustomButton>() {
                    @Override
                    public int compare(SysCustomButton o1, SysCustomButton o2) {
                        Integer n1 = StrUtil.isEmpty(o1.getSort()) ? 0 : Integer.parseInt(o1.getSort());
                        Integer n2 = StrUtil.isEmpty(o2.getSort()) ? 0 : Integer.parseInt(o2.getSort());
                        return n1 - n2;
                    }
                });

                customList.setListButton(JSONUtil.parseArray(customButtons).toString());
                sysCustomListMapper.updateSysCustomList(customList);
            }
        }
        return 1;
    }

    @Override
    public int customButtonDelete(SysCustomButton sysCustomButton) {
        List<SysCustomButton> customButtons = new ArrayList<>();
        if (StrUtil.isNotEmpty(sysCustomButton.getCustomId())) {
            SysCustomList customList = sysCustomListMapper.selectSysCustomListById(Long.parseLong(sysCustomButton.getCustomId()));
            if (customList != null) {
                if (StrUtil.isNotEmpty(customList.getListButton())) {
                    JSONArray jsonArray = JSONUtil.parseArray(customList.getListButton());
                    customButtons = JSONUtil.toList(jsonArray, SysCustomButton.class);
                    //删除按钮
                    List<SysCustomButton> customButtonsSave = new ArrayList<>();
                    for (SysCustomButton button : customButtons) {
                        if (!button.getButtonId().equals(sysCustomButton.getButtonId())) {
                            customButtonsSave.add(button);
                        }
                    }
                    customList.setListButton(JSONUtil.parseArray(customButtonsSave).toString());
                    sysCustomListMapper.updateSysCustomList(customList);
                }

            }
        }
        return 1;
    }

    @Override
    public void handleCustomFormData(String id, String buttonFunction, SysCustomList sysCustomList) {
        if ("update".equals(buttonFunction)) {
            if (StrUtil.isEmpty(id)) {
                throw new BusinessException("数据ID不能为空");
            }
            String sql = "select * from " + sysCustomList.getListTable() + " where id=" + id;
            List<Map<String, Object>> listData = sysCustomListMapper.executeCustomSql(sql);
            //组装数据
            JSONArray jsonArray = new JSONArray();
            if (listData.size() > 0) {
                Map<String, Object> map = listData.get(0);
                for (String key : map.keySet()) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("name", key);
                    jsonObject.put("value", map.get(key));
                    jsonArray.add(jsonObject);
                }
            } else {
                throw new BusinessException("无效的数据");
            }
            sysCustomList.setListValue(jsonArray.toString());
        }
    }
}
