package com.hello.custom.service;

import com.hello.common.exception.BusinessException;
import com.hello.custom.domain.SysCustomButton;
import com.hello.custom.domain.SysCustomField;
import com.hello.custom.domain.SysCustomList;

import java.util.List;
import java.util.Map;

/**
 * 自定义列Service接口
 *
 * @author lyj
 * @date 2020-12-17
 */
public interface ISysCustomListService {
    /**
     * 查询自定义列
     *
     * @param id 自定义列ID
     * @return 自定义列
     */
    public SysCustomList selectSysCustomListById(Long id);

    /**
     * 查询自定义列列表
     *
     * @param sysCustomList 自定义列
     * @return 自定义列集合
     */
    public List<SysCustomList> selectSysCustomListList(SysCustomList sysCustomList);

    /**
     * 新增自定义列
     *
     * @param sysCustomList 自定义列
     * @return 结果
     */
    public int insertSysCustomList(SysCustomList sysCustomList);

    /**
     * 修改自定义列
     *
     * @param sysCustomList 自定义列
     * @return 结果
     */
    public int updateSysCustomList(SysCustomList sysCustomList);

    /**
     * 批量删除自定义列
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysCustomListByIds(String ids);

    /**
     * 删除自定义列信息
     *
     * @param id 自定义列ID
     * @return 结果
     */
    public int deleteSysCustomListById(Long id);

    /* *
     *@description: 查询自定义sql
     *@author: lyj
     *@time: 2020/12/18 10:52
     *@param [listSQL]
     */
    public List<SysCustomField> executeCustomSql(String listSql, Long id) throws BusinessException;

    /* *
     *@description: 启用停用
     *@author: lyj
     *@time: 2020/12/28 15:39
     *@param [sysCustomList]
     */
    public int changeStatus(SysCustomList sysCustomList);

    /* *
     *@description: 根据列表编码查询列表详情
     *@author: lyj
     *@time: 2020/12/28 16:51
     *@param [listCode]
     */
    public SysCustomList selectSysCustomListByCode(String listCode);

    /* *
     *@description: 返回自定义sql查询结果
     *@author: lyj
     *@time: 2020/12/28 17:40
     *@param [sysCustomList]
     */
    public List<Map<String, Object>> selectSysCustomListData(SysCustomList sysCustomList);

    /* *
     *@description: 根据字典类型查询
     *@author: lyj
     *@time: 2020/12/30 11:28
     *@param [dictType]
     */
    public String queryDictValueByType(String dictType);

    /* *
     *@description: 自定义表单保存修改
     *@author: lyj
     *@time: 2020/12/30 17:56
     *@param [sysCustomList]
     */
    public int customFormSaveOrUpdate(SysCustomList sysCustomList);

    /* *
     *@description: 查询自定义列表按钮
     *@author: lyj
     *@time: 2021/1/7 14:53
     *@param [id]
     */
    public List<SysCustomButton> customButton(Long id);

    /* *
     *@description: 保存自定义按钮
     *@author: lyj
     *@time: 2021/1/7 16:52
     *@param [sysCustomButton]
     */
    public int customButtonSave(SysCustomButton sysCustomButton);

    /* *
     *@description: 删除自定义按钮
     *@author: lyj
     *@time: 2021/1/7 17:53
     *@param [sysCustomButton]
     */
    public int customButtonDelete(SysCustomButton sysCustomButton);

    /* *
     *@description: 处理自定义表单弹窗数据
     *@author: lyj
     *@time: 2021/1/13 17:24
     *@param [id, buttonFunction, sysCustomList]
     */
    public void handleCustomFormData(String id, String buttonFunction, SysCustomList sysCustomList);
}
