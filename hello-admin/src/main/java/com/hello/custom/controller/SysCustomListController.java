package com.hello.custom.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.hello.common.annotation.Log;
import com.hello.common.core.controller.BaseController;
import com.hello.common.core.domain.AjaxResult;
import com.hello.common.core.page.TableDataInfo;
import com.hello.common.enums.BusinessType;
import com.hello.common.exception.BusinessException;
import com.hello.common.utils.poi.ExcelUtil;
import com.hello.custom.domain.SysCustomButton;
import com.hello.custom.domain.SysCustomField;
import com.hello.custom.domain.SysCustomList;
import com.hello.custom.service.ISysCustomListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 自定义列Controller
 *
 * @author lyj
 * @date 2020-12-17
 */
@Controller
@RequestMapping("/custom/list")
public class SysCustomListController extends BaseController {
    private String prefix = "custom/list";

    @Autowired
    private ISysCustomListService sysCustomListService;

    @GetMapping()
    public String list() {
        return prefix + "/list";
    }

    /**
     * 查询自定义列列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysCustomList sysCustomList) {
        startPage();
        List<SysCustomList> list = sysCustomListService.selectSysCustomListList(sysCustomList);
        return getDataTable(list);
    }

    /**
     * 导出自定义列列表
     */
    @Log(title = "自定义列", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysCustomList sysCustomList) {
        List<SysCustomList> list = sysCustomListService.selectSysCustomListList(sysCustomList);
        ExcelUtil<SysCustomList> util = new ExcelUtil<SysCustomList>(SysCustomList.class);
        return util.exportExcel(list, "list");
    }

    /**
     * 新增自定义列
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        mmap.put("sysCustomList", new SysCustomList());
        return prefix + "/add";
    }


    /**
     * 新增保存自定义列
     */
    @Log(title = "自定义列", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysCustomList sysCustomList) {
        if (sysCustomList.getId() == null) {
            return toAjax(sysCustomListService.insertSysCustomList(sysCustomList));
        } else {
            return toAjax(sysCustomListService.updateSysCustomList(sysCustomList));
        }
    }

    /**
     * 修改自定义列
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        SysCustomList sysCustomList = sysCustomListService.selectSysCustomListById(id);
        mmap.put("sysCustomList", sysCustomList);
        return prefix + "/add";
    }

    /**
     * 修改保存自定义列
     */
    @Log(title = "自定义列", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysCustomList sysCustomList) {
        return toAjax(sysCustomListService.updateSysCustomList(sysCustomList));
    }

    /**
     * 删除自定义列
     */
    @Log(title = "自定义列", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(sysCustomListService.deleteSysCustomListByIds(ids));
    }


    @PostMapping("/customField")
    @ResponseBody
    public TableDataInfo customField(SysCustomList sysCustomList) throws BusinessException {
        String listSql = sysCustomList.getListSql();
        TableDataInfo dataInfo = new TableDataInfo();
        if (StrUtil.isNotEmpty(listSql)) {
            List<SysCustomField> list = sysCustomListService.executeCustomSql(listSql, sysCustomList.getId());
            dataInfo.setRows(list);
        }
        return dataInfo;
    }



    /* *
     *@description: 根据字典类型查询
     *@author: lyj
     *@time: 2020/12/30 11:28
     *@param [dictType]
     */
    @PostMapping("/queryDictValueByType")
    @ResponseBody
    public String queryDictValueByType(String dictType) {
        return sysCustomListService.queryDictValueByType(dictType);
    }


    @Log(title = "启用停用自定义列表", businessType = BusinessType.UPDATE)
    @PostMapping("/changeStatus")
    @ResponseBody
    public AjaxResult changeStatus(SysCustomList sysCustomList) {
        return toAjax(sysCustomListService.changeStatus(sysCustomList));
    }

    @GetMapping("/button")
    public String button(ModelMap mmap, String customId, String buttonId,String type) {
        mmap.put("sysCustomButton", new SysCustomButton());
        //回显按钮信息
        if (StrUtil.isNotEmpty(buttonId)) {
            SysCustomList customList = sysCustomListService.selectSysCustomListById(Long.parseLong(customId));
            if (StrUtil.isNotEmpty(customList.getListButton())) {
                JSONArray jsonArray = JSONUtil.parseArray(customList.getListButton());
                List<SysCustomButton> customButtons = JSONUtil.toList(jsonArray, SysCustomButton.class);
                for (SysCustomButton button : customButtons) {
                    if (button.getButtonId().equals(buttonId)) {
                        if("copy".equals(type)){
                            button.setButtonName(button.getButtonName()+"-复制");
                            button.setButtonId("");
                        }
                        mmap.put("sysCustomButton", button);
                        break;
                    }
                }
            }
        }

        mmap.put("buttonId", buttonId);
        return prefix + "/button";
    }

    @PostMapping("/customButton")
    @ResponseBody
    public TableDataInfo customButton(SysCustomList sysCustomList) throws BusinessException {
        TableDataInfo dataInfo = new TableDataInfo();
        List<SysCustomButton> list = sysCustomListService.customButton(sysCustomList.getId());
        dataInfo.setRows(list);
        return dataInfo;
    }

    @Log(title = "保存自定义按钮", businessType = BusinessType.UPDATE)
    @PostMapping("/customButtonSave")
    @ResponseBody
    public AjaxResult customButtonSave(SysCustomButton sysCustomButton) {
        return toAjax(sysCustomListService.customButtonSave(sysCustomButton));
    }

    @Log(title = "删除自定义按钮", businessType = BusinessType.DELETE)
    @PostMapping("/customButtonDelete")
    @ResponseBody
    public AjaxResult customButtonDelete(SysCustomButton sysCustomButton) {
        return toAjax(sysCustomListService.customButtonDelete(sysCustomButton));
    }


}
