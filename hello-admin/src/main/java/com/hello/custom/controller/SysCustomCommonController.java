package com.hello.custom.controller;

import cn.hutool.core.lang.Func;
import cn.hutool.core.util.StrUtil;
import com.hello.common.annotation.Log;
import com.hello.common.core.controller.BaseController;
import com.hello.common.core.domain.AjaxResult;
import com.hello.common.core.page.TableDataInfo;
import com.hello.common.enums.BusinessType;
import com.hello.common.exception.BusinessException;
import com.hello.common.utils.poi.ExcelUtil;
import com.hello.custom.domain.SysCustomButton;
import com.hello.custom.domain.SysCustomField;
import com.hello.custom.domain.SysCustomList;
import com.hello.custom.service.ISysCustomListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 自定义列Controller
 *
 * @author lyj
 * @date 2020-12-17
 */
@Controller
@RequestMapping("/custom/common/list")
public class SysCustomCommonController extends BaseController {
    private String prefix = "custom/common";

    @Autowired
    private ISysCustomListService sysCustomListService;

    @GetMapping("{listCode}")
    public String list(@PathVariable("listCode") String listCode, ModelMap modelMap) {
        SysCustomList sysCustomList = sysCustomListService.selectSysCustomListByCode(listCode);
        modelMap.put("sysCustomList", sysCustomList);
        return prefix + "/list";
    }

    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysCustomList sysCustomList) {
        startPage();
        List<Map<String, Object>> list = sysCustomListService.selectSysCustomListData(sysCustomList);
        return getDataTable(list);
    }

    @Log(title = "表单数据处理", businessType = BusinessType.INSERT)
    @PostMapping("/handleFormData")
    @ResponseBody
    public AjaxResult handleFormData(SysCustomList sysCustomList) {
        return toAjax(sysCustomListService.customFormSaveOrUpdate(sysCustomList));
    }

    @GetMapping("openCustomForm")
    public String openCustomForm(String id,String buttonFunction,String listCode, ModelMap modelMap) {
        SysCustomList sysCustomList = sysCustomListService.selectSysCustomListByCode(listCode);
        sysCustomListService.handleCustomFormData(id,buttonFunction,sysCustomList);
        modelMap.put("sysCustomList", sysCustomList);
        modelMap.put("id", id);

        return prefix + "/form";
    }

}
