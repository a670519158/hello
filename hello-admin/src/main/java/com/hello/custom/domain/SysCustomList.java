package com.hello.custom.domain;

import com.hello.common.annotation.Excel;
import com.hello.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * 自定义列对象 sys_custom_list
 *
 * @author lyj
 * @date 2020-12-17
 */
@Data
public class SysCustomList extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;

    @Excel(name="自定义主表名")
    private String listTable;
    /**
     * 自定义列表编码
     */
    @Excel(name = "自定义列表编码")
    private String listCode;

    /**
     * 自定义列表名称
     */
    @Excel(name = "自定义列表名称")
    private String listName;

    /**
     * 自定义列表内容
     */
    @Excel(name = "自定义列表内容")
    private String listValue;

    /**
     * 自定义列表sql
     */
    @Excel(name = "自定义列表sql")
    private String listSql;

    /**
     * 自定义列表按钮
     */
    @Excel(name = "自定义列表按钮")
    private String listButton;

    /**
     * 自定义列表查询
     */
    @Excel(name = "自定义列表查询")
    private String listQuery;

    @Excel(name = "自定义表单列数")
    private String formCellNum;

    @Excel(name = "自定义表单")
    private String listForm;

    @Excel(name = "自定义表单画布")
    private String listDrapForm;

    /**
     * 状态（0正常 1停用）
     */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    private List<SysCustomField> columns;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

}
