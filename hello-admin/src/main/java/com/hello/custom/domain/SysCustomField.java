package com.hello.custom.domain;

import com.hello.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * @description: SysCustomField
 * @author: lyj
 * @time: 2020/12/18 10:35
 */
@Data
public class SysCustomField {

    private String columnName;

    private String columnShowName;

    private String isUse;

    private String isHide;

    private String isSearch;

    private String dictType;

    private String sort;

}
