package com.hello.custom.domain;

import lombok.Data;

/**
 * @description: SysCustomButton
 * @author: lyj
 * @time: 2020/12/18 10:35
 */
@Data
public class SysCustomButton {

    //列表ID
    private String customId;

    //按钮ID
    private String buttonId;

    //按钮名称
    private String buttonName;

    //按钮图标
    private String buttonIcon;

    //按钮样式
    private String buttonClass;

    //默认状态
    private String defaultUse;

    //是否使用
    private String isUse;

    //按钮权限
    private String buttonPermission;

    //按钮排序
    private String sort;

    //按钮方法
    private String buttonFunction;

    //弹窗方式
    private String openType;

}
