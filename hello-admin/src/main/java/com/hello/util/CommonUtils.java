package com.hello.util;

import com.hello.common.core.domain.BaseEntity;
import com.hello.common.utils.DateUtils;
import com.hello.framework.util.ShiroUtils;

/**
 * @author lyj
 * @des 工具类
 * 2020/8/14
 */
public class CommonUtils {


    /**
     * 保存实体基础信息
     *
     * @param entity
     * @param flag   true:新增 false 修改
     */
    public static void saveBaseInfo(BaseEntity entity, boolean flag) {
        if (flag) {
            //当前用户
            entity.setCreateUserId(ShiroUtils.getUserId());
            entity.setCreateBy(ShiroUtils.getLoginName());
            //创建时间
            entity.setCreateTime(DateUtils.getNowDate());
        } else {
            //当前用户
            entity.setUpdateUserId(ShiroUtils.getUserId());
            entity.setUpdateBy(ShiroUtils.getLoginName());
            //创建时间
            entity.setUpdateTime(DateUtils.getNowDate());
        }
    }


}