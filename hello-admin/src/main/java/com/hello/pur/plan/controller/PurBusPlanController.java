package com.hello.pur.plan.controller;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hello.common.annotation.Log;
import com.hello.common.enums.BusinessType;
import com.hello.pur.plan.domain.PurBusPlan;
import com.hello.pur.plan.service.IPurBusPlanService;
import com.hello.common.core.controller.BaseController;
import com.hello.common.core.domain.AjaxResult;
import com.hello.common.utils.poi.ExcelUtil;
import com.hello.common.core.page.TableDataInfo;

/**
 * 采购计划Controller
 *
 * @author lyj
 * @date 2020-11-23
 */
@Controller
@RequestMapping("/pur/plan")
public class PurBusPlanController extends BaseController {
    private String prefix = "pur/plan";

    @Autowired
    private IPurBusPlanService purBusPlanService;

    @RequiresPermissions("pur:plan:view")
    @GetMapping()
    public String plan() {
        return prefix + "/plan";
    }

    /**
     * 查询采购计划列表
     */
    @RequiresPermissions("pur:plan:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PurBusPlan purBusPlan) {
        startPage();
        List<PurBusPlan> list = purBusPlanService.selectPurBusPlanList(purBusPlan);
        return getDataTable(list);
    }

    /**
     * 导出采购计划列表
     */
    @RequiresPermissions("pur:plan:export")
    @Log(title = "采购计划", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PurBusPlan purBusPlan) {
        List<PurBusPlan> list = purBusPlanService.selectPurBusPlanList(purBusPlan);
        ExcelUtil<PurBusPlan> util = new ExcelUtil<PurBusPlan>(PurBusPlan.class);
        return util.exportExcel(list, "plan");
    }

    /**
     * 新增采购计划
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存采购计划
     */
    @RequiresPermissions("pur:plan:add")
    @Log(title = "采购计划", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PurBusPlan purBusPlan) {
        return toAjax(purBusPlanService.insertPurBusPlan(purBusPlan));
    }

    /**
     * 修改采购计划
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        PurBusPlan purBusPlan = purBusPlanService.selectPurBusPlanById(id);
        mmap.put("purBusPlan", purBusPlan);
        return prefix + "/edit";
    }

    /**
     * 修改保存采购计划
     */
    @RequiresPermissions("pur:plan:edit")
    @Log(title = "采购计划", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PurBusPlan purBusPlan) {
        return toAjax(purBusPlanService.updatePurBusPlan(purBusPlan));
    }

    /**
     * 删除采购计划
     */
    @RequiresPermissions("pur:plan:remove")
    @Log(title = "采购计划", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(purBusPlanService.deletePurBusPlanByIds(ids));
    }

    @RequiresPermissions("pur:plan:edit")
    @Log(title = "项目预算", businessType = BusinessType.INSERT)
    @PostMapping("/changeStatus")
    @ResponseBody
    public AjaxResult changeStatus(PurBusPlan purBusPlan) {
        return toAjax(purBusPlanService.changeStatus(purBusPlan));
    }

    /* *
     *@description: 打开预算列表
     *@author: lyj
     *@time: 2020/12/3 17:01
     *@param [budgetId, mmap]
     */
    @GetMapping("/parent")
    public String parent(String budgetId, ModelMap mmap) {
        mmap.put("budgetId", budgetId);
        return prefix + "/budgetParent";
    }

    /* *
     *@description: 计划编码验重
     *@author: lyj
     *@time: 2020/12/3 17:02
     *@param [purBusPlan]
     */
    @PostMapping("/checkCode")
    @ResponseBody
    public String checkCode(PurBusPlan purBusPlan) {
        return purBusPlanService.checkCode(purBusPlan);
    }
}
