package com.hello.pur.plan.service.impl;

import java.util.List;
import com.hello.common.utils.DateUtils;
import org.springframework.stereotype.Service;
import com.hello.pur.plan.mapper.PurBusPlanMapper;
import com.hello.pur.plan.domain.PurBusPlan;
import com.hello.pur.plan.service.IPurBusPlanService;
import com.hello.common.core.text.Convert;

import javax.annotation.Resource;

/**
 * 采购计划Service业务层处理
 * 
 * @author lyj
 * @date 2020-11-23
 */
@Service
public class PurBusPlanServiceImpl implements IPurBusPlanService 
{
    @Resource
    private PurBusPlanMapper purBusPlanMapper;

    /**
     * 查询采购计划
     * 
     * @param id 采购计划ID
     * @return 采购计划
     */
    @Override
    public PurBusPlan selectPurBusPlanById(Long id)
    {
        return purBusPlanMapper.selectPurBusPlanById(id);
    }

    /**
     * 查询采购计划列表
     * 
     * @param purBusPlan 采购计划
     * @return 采购计划
     */
    @Override
    public List<PurBusPlan> selectPurBusPlanList(PurBusPlan purBusPlan)
    {
        return purBusPlanMapper.selectPurBusPlanList(purBusPlan);
    }

    /**
     * 新增采购计划
     * 
     * @param purBusPlan 采购计划
     * @return 结果
     */
    @Override
    public int insertPurBusPlan(PurBusPlan purBusPlan)
    {
        purBusPlan.setCreateTime(DateUtils.getNowDate());
        return purBusPlanMapper.insertPurBusPlan(purBusPlan);
    }

    /**
     * 修改采购计划
     * 
     * @param purBusPlan 采购计划
     * @return 结果
     */
    @Override
    public int updatePurBusPlan(PurBusPlan purBusPlan)
    {
        purBusPlan.setUpdateTime(DateUtils.getNowDate());
        return purBusPlanMapper.updatePurBusPlan(purBusPlan);
    }

    /**
     * 删除采购计划对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePurBusPlanByIds(String ids)
    {
        return purBusPlanMapper.deletePurBusPlanByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除采购计划信息
     * 
     * @param id 采购计划ID
     * @return 结果
     */
    @Override
    public int deletePurBusPlanById(Long id)
    {
        return purBusPlanMapper.deletePurBusPlanById(id);
    }

    @Override
    public String checkCode(PurBusPlan purBusPlan) {
        int count = purBusPlanMapper.checkCode(purBusPlan.getPlanCode());
        if (count > 0) {
            return "1";
        }
        return "0";
    }

    @Override
    public int changeStatus(PurBusPlan purBusPlan) {
        return purBusPlanMapper.updatePurBusPlan(purBusPlan);
    }
}
