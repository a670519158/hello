package com.hello.pur.plan.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hello.common.annotation.Excel;
import com.hello.common.core.domain.BaseEntity;

/**
 * 采购计划对象 pur_bus_plan
 * 
 * @author lyj
 * @date 2020-11-23
 */
public class PurBusPlan extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 采购计划编码 */
    @Excel(name = "采购计划编码")
    private String planCode;

    /** 采购计划名称 */
    @Excel(name = "采购计划名称")
    private String planName;

    /** 申报年度 */
    @Excel(name = "申报年度")
    private String planYear;

    /** 采购方式 */
    @Excel(name = "采购方式")
    private String purWay;

    /** 单价 */
    @Excel(name = "单价")
    private BigDecimal planPrice;

    /** 数量 */
    @Excel(name = "数量")
    private Long planNum;

    /** 采购金额 */
    @Excel(name = "采购金额")
    private BigDecimal planMoney;

    /** 预算ID */
    private Long budgetId;

    /** 项目预算编码 */
    @Excel(name = "项目预算编码")
    private String budgetCode;

    /** 项目预算名称 */
    @Excel(name = "项目预算名称")
    private String budgetName;

    /** 预算指标 */
    @Excel(name = "预算指标")
    private BigDecimal budgetQuota;

    /** 用户ID */
    private Long userId;

    /** 用户昵称 */
    @Excel(name = "用户昵称")
    private String userName;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPlanCode(String planCode) 
    {
        this.planCode = planCode;
    }

    public String getPlanCode() 
    {
        return planCode;
    }
    public void setPlanName(String planName) 
    {
        this.planName = planName;
    }

    public String getPlanName() 
    {
        return planName;
    }
    public void setPlanYear(String planYear) 
    {
        this.planYear = planYear;
    }

    public String getPlanYear() 
    {
        return planYear;
    }
    public void setPurWay(String purWay) 
    {
        this.purWay = purWay;
    }

    public String getPurWay() 
    {
        return purWay;
    }
    public void setPlanPrice(BigDecimal planPrice) 
    {
        this.planPrice = planPrice;
    }

    public BigDecimal getPlanPrice() 
    {
        return planPrice;
    }
    public void setPlanNum(Long planNum) 
    {
        this.planNum = planNum;
    }

    public Long getPlanNum() 
    {
        return planNum;
    }
    public void setPlanMoney(BigDecimal planMoney) 
    {
        this.planMoney = planMoney;
    }

    public BigDecimal getPlanMoney() 
    {
        return planMoney;
    }
    public void setBudgetId(Long budgetId) 
    {
        this.budgetId = budgetId;
    }

    public Long getBudgetId() 
    {
        return budgetId;
    }
    public void setBudgetCode(String budgetCode) 
    {
        this.budgetCode = budgetCode;
    }

    public String getBudgetCode() 
    {
        return budgetCode;
    }
    public void setBudgetName(String budgetName) 
    {
        this.budgetName = budgetName;
    }

    public String getBudgetName() 
    {
        return budgetName;
    }
    public void setBudgetQuota(BigDecimal budgetQuota) 
    {
        this.budgetQuota = budgetQuota;
    }

    public BigDecimal getBudgetQuota() 
    {
        return budgetQuota;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("planCode", getPlanCode())
            .append("planName", getPlanName())
            .append("planYear", getPlanYear())
            .append("purWay", getPurWay())
            .append("planPrice", getPlanPrice())
            .append("planNum", getPlanNum())
            .append("planMoney", getPlanMoney())
            .append("budgetId", getBudgetId())
            .append("budgetCode", getBudgetCode())
            .append("budgetName", getBudgetName())
            .append("budgetQuota", getBudgetQuota())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
