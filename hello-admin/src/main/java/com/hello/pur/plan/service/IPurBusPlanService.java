package com.hello.pur.plan.service;

import java.util.List;

import com.hello.pur.plan.domain.PurBusPlan;

/**
 * 采购计划Service接口
 * 
 * @author lyj
 * @date 2020-11-23
 */
public interface IPurBusPlanService 
{
    /**
     * 查询采购计划
     * 
     * @param id 采购计划ID
     * @return 采购计划
     */
    public PurBusPlan selectPurBusPlanById(Long id);

    /**
     * 查询采购计划列表
     * 
     * @param purBusPlan 采购计划
     * @return 采购计划集合
     */
    public List<PurBusPlan> selectPurBusPlanList(PurBusPlan purBusPlan);

    /**
     * 新增采购计划
     * 
     * @param purBusPlan 采购计划
     * @return 结果
     */
    public int insertPurBusPlan(PurBusPlan purBusPlan);

    /**
     * 修改采购计划
     * 
     * @param purBusPlan 采购计划
     * @return 结果
     */
    public int updatePurBusPlan(PurBusPlan purBusPlan);

    /**
     * 批量删除采购计划
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePurBusPlanByIds(String ids);

    /**
     * 删除采购计划信息
     * 
     * @param id 采购计划ID
     * @return 结果
     */
    public int deletePurBusPlanById(Long id);

    /* *
     *@description: 重复编码校验
     *@author: lyj
     *@time: 2020/11/23 16:18
     *@param [purBusPlan]
     */
    public String checkCode(PurBusPlan purBusPlan);

    public int changeStatus(PurBusPlan purBusPlan);
}
