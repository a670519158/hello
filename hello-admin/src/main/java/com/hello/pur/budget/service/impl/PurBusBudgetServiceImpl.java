package com.hello.pur.budget.service.impl;

import com.hello.common.core.text.Convert;
import com.hello.common.utils.DateUtils;
import com.hello.pur.budget.domain.PurBusBudget;
import com.hello.pur.budget.mapper.PurBusBudgetMapper;
import com.hello.pur.budget.service.IPurBusBudgetService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 项目预算Service业务层处理
 *
 * @author lyj
 * @date 2020-11-23
 */
@Service
public class PurBusBudgetServiceImpl implements IPurBusBudgetService {

    @Resource
    private PurBusBudgetMapper purBusBudgetMapper;

    /**
     * 查询项目预算
     *
     * @param id 项目预算ID
     * @return 项目预算
     */
    @Override
    public PurBusBudget selectPurBusBudgetById(Long id) {
        return purBusBudgetMapper.selectPurBusBudgetById(id);
    }

    /**
     * 查询项目预算列表
     *
     * @param purBusBudget 项目预算
     * @return 项目预算
     */
    @Override
    public List<PurBusBudget> selectPurBusBudgetList(PurBusBudget purBusBudget) {
        return purBusBudgetMapper.selectPurBusBudgetList(purBusBudget);
    }

    /**
     * 新增项目预算
     *
     * @param purBusBudget 项目预算
     * @return 结果
     */
    @Override
    public int insertPurBusBudget(PurBusBudget purBusBudget) {
        purBusBudget.setCreateTime(DateUtils.getNowDate());
        return purBusBudgetMapper.insertPurBusBudget(purBusBudget);
    }

    /**
     * 修改项目预算
     *
     * @param purBusBudget 项目预算
     * @return 结果
     */
    @Override
    public int updatePurBusBudget(PurBusBudget purBusBudget) {
        purBusBudget.setUpdateTime(DateUtils.getNowDate());
        System.out.println("123");
        return purBusBudgetMapper.updatePurBusBudget(purBusBudget);
    }

    /**
     * 删除项目预算对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePurBusBudgetByIds(String ids) {
        return purBusBudgetMapper.deletePurBusBudgetByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除项目预算信息
     *
     * @param id 项目预算ID
     * @return 结果
     */
    @Override
    public int deletePurBusBudgetById(Long id) {
        return purBusBudgetMapper.deletePurBusBudgetById(id);
    }

    @Override
    public int changeStatus(PurBusBudget purBusBudget) {
        return purBusBudgetMapper.updatePurBusBudget(purBusBudget);
    }

    @Override
    public String checkCode(PurBusBudget purBusBudget) {
        int count = purBusBudgetMapper.checkCode(purBusBudget.getBudgetCode());
        if (count > 0) {
            return "1";
        }
        return "0";
    }

    @Override
    public List<PurBusBudget> queryUseableBudgetList(PurBusBudget purBusBudget) {
        return purBusBudgetMapper.queryUseableBudgetList(purBusBudget);
    }
}
