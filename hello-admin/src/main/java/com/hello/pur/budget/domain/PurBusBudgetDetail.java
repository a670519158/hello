package com.hello.pur.budget.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hello.common.annotation.Excel;
import com.hello.common.core.domain.BaseEntity;

/**
 * 预算明细对象 pur_bus_budget_detail
 * 
 * @author lyj
 * @date 2020-12-07
 */
public class PurBusBudgetDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 采购计划编码 */
    @Excel(name = "采购计划编码")
    private String planCode;

    /** 采购计划名称 */
    @Excel(name = "采购计划名称")
    private String planName;

    /** 采购金额 */
    @Excel(name = "采购金额")
    private BigDecimal planMoney;

    /** 预算ID */
    @Excel(name = "预算ID")
    private Long budgetId;

    /** 项目预算编码 */
    @Excel(name = "项目预算编码")
    private String budgetCode;

    /** 项目预算名称 */
    @Excel(name = "项目预算名称")
    private String budgetName;

    /** 预算指标 */
    @Excel(name = "预算指标")
    private BigDecimal budgetQuota;

    /** 使用金额 */
    @Excel(name = "使用金额")
    private BigDecimal useMoney;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPlanCode(String planCode) 
    {
        this.planCode = planCode;
    }

    public String getPlanCode() 
    {
        return planCode;
    }
    public void setPlanName(String planName) 
    {
        this.planName = planName;
    }

    public String getPlanName() 
    {
        return planName;
    }
    public void setPlanMoney(BigDecimal planMoney) 
    {
        this.planMoney = planMoney;
    }

    public BigDecimal getPlanMoney() 
    {
        return planMoney;
    }
    public void setBudgetId(Long budgetId) 
    {
        this.budgetId = budgetId;
    }

    public Long getBudgetId() 
    {
        return budgetId;
    }
    public void setBudgetCode(String budgetCode) 
    {
        this.budgetCode = budgetCode;
    }

    public String getBudgetCode() 
    {
        return budgetCode;
    }
    public void setBudgetName(String budgetName) 
    {
        this.budgetName = budgetName;
    }

    public String getBudgetName() 
    {
        return budgetName;
    }
    public void setBudgetQuota(BigDecimal budgetQuota) 
    {
        this.budgetQuota = budgetQuota;
    }

    public BigDecimal getBudgetQuota() 
    {
        return budgetQuota;
    }
    public void setUseMoney(BigDecimal useMoney) 
    {
        this.useMoney = useMoney;
    }

    public BigDecimal getUseMoney() 
    {
        return useMoney;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("planCode", getPlanCode())
            .append("planName", getPlanName())
            .append("planMoney", getPlanMoney())
            .append("budgetId", getBudgetId())
            .append("budgetCode", getBudgetCode())
            .append("budgetName", getBudgetName())
            .append("budgetQuota", getBudgetQuota())
            .append("useMoney", getUseMoney())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
