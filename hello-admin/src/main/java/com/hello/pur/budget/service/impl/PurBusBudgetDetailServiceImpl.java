package com.hello.pur.budget.service.impl;

import com.hello.common.core.text.Convert;
import com.hello.common.utils.DateUtils;
import com.hello.pur.budget.domain.PurBusBudgetDetail;
import com.hello.pur.budget.mapper.PurBusBudgetDetailMapper;
import com.hello.pur.budget.service.IPurBusBudgetDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 预算明细Service业务层处理
 *
 * @author lyj
 * @date 2020-12-07
 */
@Service
public class PurBusBudgetDetailServiceImpl implements IPurBusBudgetDetailService {
    @Resource
    private PurBusBudgetDetailMapper purBusBudgetDetailMapper;

    /**
     * 查询预算明细
     *
     * @param id 预算明细ID
     * @return 预算明细
     */
    @Override
    public PurBusBudgetDetail selectPurBusBudgetDetailById(Long id) {
        return purBusBudgetDetailMapper.selectPurBusBudgetDetailById(id);
    }

    /**
     * 查询预算明细列表
     *
     * @param purBusBudgetDetail 预算明细
     * @return 预算明细
     */
    @Override
    public List<PurBusBudgetDetail> selectPurBusBudgetDetailList(PurBusBudgetDetail purBusBudgetDetail) {
        return purBusBudgetDetailMapper.selectPurBusBudgetDetailList(purBusBudgetDetail);
    }

    /**
     * 新增预算明细
     *
     * @param purBusBudgetDetail 预算明细
     * @return 结果
     */
    @Override
    public int insertPurBusBudgetDetail(PurBusBudgetDetail purBusBudgetDetail) {
        purBusBudgetDetail.setCreateTime(DateUtils.getNowDate());
        return purBusBudgetDetailMapper.insertPurBusBudgetDetail(purBusBudgetDetail);
    }

    /**
     * 修改预算明细
     *
     * @param purBusBudgetDetail 预算明细
     * @return 结果
     */
    @Override
    public int updatePurBusBudgetDetail(PurBusBudgetDetail purBusBudgetDetail) {
        purBusBudgetDetail.setUpdateTime(DateUtils.getNowDate());
        return purBusBudgetDetailMapper.updatePurBusBudgetDetail(purBusBudgetDetail);
    }

    /**
     * 删除预算明细对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePurBusBudgetDetailByIds(String ids) {
        return purBusBudgetDetailMapper.deletePurBusBudgetDetailByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除预算明细信息
     *
     * @param id 预算明细ID
     * @return 结果
     */
    @Override
    public int deletePurBusBudgetDetailById(Long id) {
        return purBusBudgetDetailMapper.deletePurBusBudgetDetailById(id);
    }
}
