package com.hello.pur.budget.mapper;

import java.util.List;

import com.hello.pur.budget.domain.PurBusBudget;

/**
 * 项目预算Mapper接口
 *
 * @author lyj
 * @date 2020-11-23
 */
public interface PurBusBudgetMapper {
    /**
     * 查询项目预算
     *
     * @param id 项目预算ID
     * @return 项目预算
     */
    public PurBusBudget selectPurBusBudgetById(Long id);

    /**
     * 查询项目预算列表
     *
     * @param purBusBudget 项目预算
     * @return 项目预算集合
     */
    public List<PurBusBudget> selectPurBusBudgetList(PurBusBudget purBusBudget);

    /**
     * 新增项目预算
     *
     * @param purBusBudget 项目预算
     * @return 结果
     */
    public int insertPurBusBudget(PurBusBudget purBusBudget);

    /**
     * 修改项目预算
     *
     * @param purBusBudget 项目预算
     * @return 结果
     */
    public int updatePurBusBudget(PurBusBudget purBusBudget);

    /**
     * 删除项目预算
     *
     * @param id 项目预算ID
     * @return 结果
     */
    public int deletePurBusBudgetById(Long id);

    /**
     * 批量删除项目预算
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePurBusBudgetByIds(String[] ids);

    /* *
     *@description: 根据编码查询
     *@author: lyj
     *@time: 2020/11/23 16:49
     *@param [budgetCode]
     */
    public int checkCode(String budgetCode);

    /* *
     *@description: 查询可用预算列表
     *@author: lyj
     *@time: 2020/12/3 16:41
     *@param [purBusBudget]
     */
    public List<PurBusBudget> queryUseableBudgetList(PurBusBudget purBusBudget);
}
