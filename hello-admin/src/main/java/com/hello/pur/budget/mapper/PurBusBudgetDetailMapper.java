package com.hello.pur.budget.mapper;

import java.util.List;
import com.hello.pur.budget.domain.PurBusBudgetDetail;

/**
 * 预算明细Mapper接口
 * 
 * @author lyj
 * @date 2020-12-07
 */
public interface PurBusBudgetDetailMapper 
{
    /**
     * 查询预算明细
     * 
     * @param id 预算明细ID
     * @return 预算明细
     */
    public PurBusBudgetDetail selectPurBusBudgetDetailById(Long id);

    /**
     * 查询预算明细列表
     * 
     * @param purBusBudgetDetail 预算明细
     * @return 预算明细集合
     */
    public List<PurBusBudgetDetail> selectPurBusBudgetDetailList(PurBusBudgetDetail purBusBudgetDetail);

    /**
     * 新增预算明细
     * 
     * @param purBusBudgetDetail 预算明细
     * @return 结果
     */
    public int insertPurBusBudgetDetail(PurBusBudgetDetail purBusBudgetDetail);

    /**
     * 修改预算明细
     * 
     * @param purBusBudgetDetail 预算明细
     * @return 结果
     */
    public int updatePurBusBudgetDetail(PurBusBudgetDetail purBusBudgetDetail);

    /**
     * 删除预算明细
     * 
     * @param id 预算明细ID
     * @return 结果
     */
    public int deletePurBusBudgetDetailById(Long id);

    /**
     * 批量删除预算明细
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePurBusBudgetDetailByIds(String[] ids);
}
