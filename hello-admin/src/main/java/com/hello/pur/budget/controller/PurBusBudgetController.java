package com.hello.pur.budget.controller;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hello.common.annotation.Log;
import com.hello.common.enums.BusinessType;
import com.hello.pur.budget.domain.PurBusBudget;
import com.hello.pur.budget.service.IPurBusBudgetService;
import com.hello.common.core.controller.BaseController;
import com.hello.common.core.domain.AjaxResult;
import com.hello.common.utils.poi.ExcelUtil;
import com.hello.common.core.page.TableDataInfo;

/**
 * 项目预算Controller
 *
 * @author lyj
 * @date 2020-11-23
 */
@Controller
@RequestMapping("/pur/budget")
public class PurBusBudgetController extends BaseController {
    private String prefix = "pur/budget";

    @Autowired
    private IPurBusBudgetService purBusBudgetService;

    @RequiresPermissions("pur:budget:view")
    @GetMapping()
    public String budget() {
        return prefix + "/budget";
    }

    /**
     * 查询项目预算列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PurBusBudget purBusBudget) {
        startPage();
        List<PurBusBudget> list = purBusBudgetService.selectPurBusBudgetList(purBusBudget);
        return getDataTable(list);
    }

    @PostMapping("/queryUseableBudgetList")
    @ResponseBody
    public TableDataInfo queryUseableBudgetList(PurBusBudget purBusBudget) {
        startPage();
        List<PurBusBudget> list = purBusBudgetService.queryUseableBudgetList(purBusBudget);
        return getDataTable(list);
    }

    /**
     * 导出项目预算列表
     */
    @RequiresPermissions("pur:budget:export")
    @Log(title = "项目预算", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PurBusBudget purBusBudget) {
        List<PurBusBudget> list = purBusBudgetService.selectPurBusBudgetList(purBusBudget);
        ExcelUtil<PurBusBudget> util = new ExcelUtil<PurBusBudget>(PurBusBudget.class);
        return util.exportExcel(list, "budget");
    }

    /**
     * 新增项目预算
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存项目预算
     */
    @RequiresPermissions("pur:budget:add")
    @Log(title = "项目预算", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PurBusBudget purBusBudget) {
        return toAjax(purBusBudgetService.insertPurBusBudget(purBusBudget));
    }

    /**
     * 修改项目预算
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        PurBusBudget purBusBudget = purBusBudgetService.selectPurBusBudgetById(id);
        mmap.put("purBusBudget", purBusBudget);
        return prefix + "/edit";
    }

    /**
     * 修改保存项目预算
     */
    @RequiresPermissions("pur:budget:edit")
    @Log(title = "项目预算", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PurBusBudget purBusBudget) {
        return toAjax(purBusBudgetService.updatePurBusBudget(purBusBudget));
    }

    /**
     * 删除项目预算
     */
    @RequiresPermissions("pur:budget:remove")
    @Log(title = "项目预算", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(purBusBudgetService.deletePurBusBudgetByIds(ids));
    }


    @RequiresPermissions("pur:budget:edit")
    @Log(title = "项目预算", businessType = BusinessType.INSERT)
    @PostMapping("/changeStatus")
    @ResponseBody
    public AjaxResult changeStatus(PurBusBudget purBusBudget) {
        return toAjax(purBusBudgetService.changeStatus(purBusBudget));
    }

    /* *
     *@description: 编码校验
     *@author: lyj
     *@time: 2020/11/23 16:17
     *@param [purBusBudget]
     */
    @PostMapping("/checkCode")
    @ResponseBody
    public String checkCode(PurBusBudget purBusBudget) {
        return purBusBudgetService.checkCode(purBusBudget);
    }

    @RequiresPermissions("pur:budget:view")
    @PostMapping("/budgetDetail")
    public String budgetDetail(String budgetId) {
        return prefix + "/budgetDetail";
    }
}
