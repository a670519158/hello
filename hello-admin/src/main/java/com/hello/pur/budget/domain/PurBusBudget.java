package com.hello.pur.budget.domain;

import java.math.BigDecimal;
import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hello.common.annotation.Excel;
import com.hello.common.core.domain.BaseEntity;

/**
 * 预算对象 pur_bus_budget
 * 
 * @author lyj
 * @date 2020-11-23
 */
public class PurBusBudget extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 预算ID */
    private Long id;

    /** 预算编码 */
    @Excel(name = "预算编码")
    private String budgetCode;

    /** 预算名称 */
    @Excel(name = "预算名称")
    private String budgetName;

    /** 预算指标 */
    @Excel(name = "预算指标")
    private BigDecimal budgetQuota;

    /** 预算年度 */
    @Excel(name = "预算年度")
    private String budgetYear;

    /** 责任部门 */
    private Long departId;

    /** 责任部门 */
    @Excel(name = "责任部门")
    private String departName;

    /** 是否采购 */
    @Excel(name = "是否采购")
    private String isPurchase;

    /** 需要时间 */
    @Excel(name = "需要时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date needTime;

    /** 帐号状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBudgetCode(String budgetCode) 
    {
        this.budgetCode = budgetCode;
    }

    public String getBudgetCode() 
    {
        return budgetCode;
    }
    public void setBudgetName(String budgetName) 
    {
        this.budgetName = budgetName;
    }

    public String getBudgetName() 
    {
        return budgetName;
    }
    public void setBudgetQuota(BigDecimal budgetQuota) 
    {
        this.budgetQuota = budgetQuota;
    }

    public BigDecimal getBudgetQuota() 
    {
        return budgetQuota;
    }
    public void setBudgetYear(String budgetYear) 
    {
        this.budgetYear = budgetYear;
    }

    public String getBudgetYear() 
    {
        return budgetYear;
    }
    public void setDepartId(Long departId) 
    {
        this.departId = departId;
    }

    public Long getDepartId() 
    {
        return departId;
    }
    public void setDepartName(String departName) 
    {
        this.departName = departName;
    }

    public String getDepartName() 
    {
        return departName;
    }
    public void setIsPurchase(String isPurchase) 
    {
        this.isPurchase = isPurchase;
    }

    public String getIsPurchase() 
    {
        return isPurchase;
    }
    public void setNeedTime(Date needTime) 
    {
        this.needTime = needTime;
    }

    public Date getNeedTime() 
    {
        return needTime;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("budgetCode", getBudgetCode())
            .append("budgetName", getBudgetName())
            .append("budgetQuota", getBudgetQuota())
            .append("budgetYear", getBudgetYear())
            .append("departId", getDepartId())
            .append("departName", getDepartName())
            .append("isPurchase", getIsPurchase())
            .append("needTime", getNeedTime())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
